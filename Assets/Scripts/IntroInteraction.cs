﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroInteraction : MonoBehaviour
{
    //Class vars
    [SerializeField] private string sceneName;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneMngr.LoadScene(sceneName);
        }
    }
}
